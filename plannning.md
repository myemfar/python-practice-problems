# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

## planning for problems 01 and 04 done in comments in the problem file as planning.md was not seen

### 06 can_skydive

## request input of age, and confirmation of signed consent form
## compare age to >= 18, and signed consent form == True
## return value if both checks pass

### problem 09
# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

## Take an input string, and check to see if the reversed string == the input string
## will want a return variable, and a reversed string variable
## set return variable True if reversed = input word
## else return false

### problem 10

# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

## taking one input from user, a number
## we want 2 outputs, one for % 3 == 0 and one for all other states
## if first condition is met, return the string "fizz", otherwise return the input
