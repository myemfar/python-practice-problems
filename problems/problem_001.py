# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    # create return variable with no value
    lower_value = 0
    # compare value1 and value1
    # check for one lower than the other
    if value1 < value2:
        lower_value = value1
    else:
        lower_value = value2
    # prompt specifies return either if they are the same, so else condition is fine here
    # return created return variable
    return lower_value
