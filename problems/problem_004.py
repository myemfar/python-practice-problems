# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    #create max_value to store value we want to return. default value1
    #because we can return any value in the event of a tie
    max_value = value1
    #compare the first two values, replace max_value if greater than or equal to value1
    if value2 >= max_value:
        max_value = value2
    #compare value 3 to the current max_value, replace if greater or equal to.
    if value3 >= max_value:
        max_value = value3
    #return the result of comparison
    return max_value
