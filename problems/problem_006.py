# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    # set blank variables to hold return value, age confirmation and consent confirmation
    skydive_permission = False
    old_enough = False
    consent_form = False
    # pull age input, compare to >= 18, set old_enough true if age is appropriate
    if age >= 18:
        old_enough = True
    # pull consent form, change variable if consent form signed
    if has_consent_form == True:
        consent_form = True
    # set return skydive_permission if both previous variables are also true
    if age == True or consent_form == True:
        skydive_permission = True
    return skydive_permission
