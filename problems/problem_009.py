# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    #create two variables, one for the return value and one for reversed string
    palindrome = False
    rev_word = reversed(word)
    #join the reversed word to turn it from a set back to a single string
    str_word = "".join(rev_word)
    #compare this new string to the input to check for palindrome, set True if True

    if str_word == word:
        palindrome = True
    return palindrome

# pal_req = input("Gimme a word")
# result = is_palindrome(pal_req)
# print(f"If you asked me if this was a palindrome, i would say {result}")
