# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    #set default response to change if condition is met
    response = number
    #set variable to remainder of input number
    remainder = int(number) % 3
    #change response if number is divisible by 3
    if remainder == 0:
        response = "fizz"
    return response
userinp = input("gimme number")
result = is_divisible_by_3(userinp)
print(result)
