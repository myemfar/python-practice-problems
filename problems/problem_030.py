# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) == 0 or len(values) == 1:
        return None
    largest = values[0]
    sec_largest = values[1]
    for value in values:
        if value > largest:
            sec_largest = largest
            largest = value
    return sec_largest
# test1 = [0,1,4,7,0,11,-1,3]
# test2 = [12,10,8,6,5]
# test3 = [0]
# test4 = [1,1]
# print(find_second_largest(test1))
# print(find_second_largest(test2))
# print(find_second_largest(test3))
# print(find_second_largest(test4))
