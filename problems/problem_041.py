# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

# def add_csv_lines(csv_lines):
#     output = []
#     for set in range(len(csv_lines)):
#         sum = 0
#         for value in csv_lines[set]:
#             print(value)
#             if value.isdigit() == True:
#                 sum += int(value)
#         output.append(sum)
#         sum = 0
#     return output
# NOTE: was trying to be clever and write the code without breaking apart CSVs,
# the above worked but it only works if the input only has single-digit numbers in it :)

def add_csv_lines(csv_lines):
    output = []
    for set in csv_lines:
        sum = 0
        numbers = set.split(",")
        for num in numbers:
            sum += int(num)
        output.append(sum)
    return output
# test = add_csv_lines(["8,1,7,100", "0,5,10,10,10", "8,1,2,3"])
# print(test)
