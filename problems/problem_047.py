# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower_check = False
    upper_check = False
    special_check = False
    digit_check = False
    length_check = False
    accepted_characters = ["$", "!", "@"]
    if len(password) >= 6 and len(password) <=12:
        length_check = True
    for character in password:
        if character in accepted_characters:
            special_check = True
        if character.isdigit():
            digit_check = True
        if character.isalpha():
            if character.isupper():
                upper_check = True
            if character.islower():
                lower_check = True
    if length_check == True and special_check == True and upper_check == True and lower_check == True and digit_check == True:
        return True
    else:
        return False

# print(check_password("Fg!!fg"))
