# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]


#length of list = len(input)
#halfway point = len(input) // 2 + len(input) % 2
#first half is 0 to halfway
#second half is halfway to end
def halve_the_list(input_list):
    halfway_point = len(input_list) // 2 + len(input_list) % 2
    first_half = []
    second_half = []
    for num in range(halfway_point):
        first_half.append(input_list[num])
    for num in (range(len(input_list) // 2)):
        start_point = num + halfway_point
        second_half.append(input_list[start_point])
    return first_half, second_half
# input_list = [0,1,2,3,4,5,6,7,8,9]
# print(halve_the_list(input_list))
