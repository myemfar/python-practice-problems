# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(number):
    sum = 0
    endpoint = int(number)+1
    for num in range(endpoint):
        sum = sum + (int(num)/(int(num)+1))
    return sum
# test = input("gimme num")
# print(sum_fraction_sequence(test))
