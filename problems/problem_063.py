# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    output = ""
    for letter in string:
        if ord(letter) == 90:
            output += chr(65)
        elif ord(letter) == 122:
            output += chr(97)
        else:
            output += chr(1 + ord(letter))
    return output
# print(ord("Z"))
# print(ord("A"))
# print(ord("z"))
# print(ord("a"))
# print(shift_letters("abcdZXz"))
